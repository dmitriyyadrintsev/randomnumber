package RandomNumber;

public class GuessGame {
    Player playerOne = new Player();
    Player playerTwo = new Player();
    Player playerThree = new Player();

    public void startGame() {
        int guessp1 = 0;
        int guessp2 = 0;
        int guessp3 = 0;
        boolean p1isRight = false;
        boolean p2isRight = false;
        boolean p3isRight = false;
        int targetNumber = (int) (Math.random() * 10);
        System.out.println("Загаданное число от 0 до 9 . .");
        while (true) {

            playerOne.guess();
            playerTwo.guess();
            playerThree.guess();

            guessp1 = playerOne.number;
            System.out.println("Первый игрок предполагает , что это число - " + guessp1);
            guessp2 = playerTwo.number;
            System.out.println("Второй игрок предполагает , что это число - " + guessp2);
            guessp3 = playerThree.number;
            System.out.println("Третий игрок предполагает , что это число - " + guessp3);

            if (guessp1 == targetNumber) {
                p1isRight = true;
            }
            if (guessp2 == targetNumber) {
                p2isRight = true;
            }
            if (guessp3 == targetNumber) {
                p3isRight = true;
            }
            if (p1isRight || p2isRight || p3isRight) {
                System.out.println("Победитель!");
                System.out.println("Первый игрок угадал? " + p1isRight);
                System.out.println("Наверное второй игрок угадал? " + p2isRight);
                System.out.println("А может третий игрок угадал?" + p3isRight);
                System.out.println("Игра окончена");
                break;
            } else {
                System.out.println("Не угадали,загадывайте ещё раз.");
            }
        }
    }
}